variable "name" {
  type = string
}

variable "chart_repository_uri" {
  type = string
}

variable "chart_repository_username" {
  type = string
}

variable "chart_repository_password" {
  type      = string
  sensitive = true
}

variable "image_registry_server" {
  type = string
}

variable "image_registry_username" {
  type = string
}

variable "image_registry_password" {
  type      = string
  sensitive = true
}

variable "db_type" {
  type = string
}

variable "db_host" {
  type = string
}

variable "db_port" {
  type = number
}

variable "db_user" {
  type = string
}

variable "db_password" {
  type      = string
  sensitive = true
}

variable "db_database" {
  type = string
}

variable "storage_host" {
  type = string
}

variable "storage_port" {
  type = number
}

variable "storage_access_key" {
  type = string
}

variable "storage_secret_key" {
  type      = string
  sensitive = true
}

variable "artifacts_destination" {
  type = string
}
