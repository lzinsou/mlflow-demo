terraform {
  required_providers {
    helm = {
      source  = "hashicorp/helm"
      version = "2.6.0"
    }
  }
}

resource "helm_release" "mlflow" {
  name = var.name

  repository          = var.chart_repository_uri
  repository_username = var.chart_repository_username
  repository_password = var.chart_repository_password

  chart   = "mlflow"
  version = "1.7.5"

  set {
    name  = "imageCredentials.registry"
    value = var.image_registry_server
  }

  set {
    name  = "imageCredentials.username"
    value = var.image_registry_username
  }

  set_sensitive {
    name  = "imageCredentials.password"
    value = var.image_registry_password
  }

  set {
    name  = "db.type"
    value = var.db_type
  }

  set {
    name  = "db.host"
    value = var.db_host
  }

  set {
    name  = "db.port"
    value = var.db_port
  }

  set {
    name  = "db.user"
    value = var.db_user
  }

  set_sensitive {
    name  = "db.password"
    value = var.db_password
  }

  set {
    name  = "db.database"
    value = var.db_database
  }

  set {
    name  = "minio.url"
    value = "http://${var.storage_host}:${var.storage_port}"
  }

  set {
    name  = "minio.accesskey"
    value = var.storage_access_key
  }

  set_sensitive {
    name  = "minio.secretkey"
    value = var.storage_secret_key
  }

  set {
    name  = "artifactsDestination"
    value = var.artifacts_destination
  }
}
