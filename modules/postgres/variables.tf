variable "name" {
  type = string
}

variable "username" {
  type = string
}

variable "password" {
  type      = string
  sensitive = true
}

variable "database" {
  type = string
}

variable "port" {
  type = number
}
