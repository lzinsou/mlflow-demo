terraform {
  required_providers {
    kind = {
      source  = "tehcyx/kind"
      version = "0.0.13"
    }
  }
}

resource "kind_cluster" "default" {
  name            = "my-cluster"
  kubeconfig_path = pathexpand("~/.kube/config")
}
