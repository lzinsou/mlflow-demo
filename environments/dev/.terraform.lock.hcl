# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/aminueza/minio" {
  version     = "1.5.2"
  constraints = "1.5.2"
  hashes = [
    "h1:rAngD3TtfUD9ZohzFo/QJe+2ae+kTmAzn4N2n6ayOOo=",
    "zh:02d90c08a5d8efddb7388eb74dae1722823a9bc6e54fc9e7e63a711108d20d58",
    "zh:03e541c770dc7785502ab6080e62328475a1af147957b3f2761750bf4c6cbf56",
    "zh:172b7538cef2ba2ccd79b5a84dcd27a88fbbe16f5fb1b9e3d2b6a05d63e17b29",
    "zh:3becdb228c2f78333265fec82abd16b1a14d86931cec96ff2726b5beafc290ac",
    "zh:4a0066767025a94b452fad7c0c58aa3103c1c521745b6c301ca64caa874501a2",
    "zh:4cd019edc99c6884d755f0f35122cb18bdd50ffab3a0690ecde139d2224d849f",
    "zh:5430f4d3cf757205d6b2ea82640475d0b164c61da9b1b944c3e66f829ed2eb82",
    "zh:882efeb742a3f1112034c05031b4ad686dda08ed7c6db6168339433b125288c4",
    "zh:99ad9e466ebabf83208a358230b871aa1c3576224a9112ee1038591a5e2154dc",
    "zh:9e06cefd61da232ca59c02e4a3d007c782c607af81f1640ef256ded367dd1730",
    "zh:c711cbd29a5d10411ee8510bf7ed49955af8d52c77bd3983f5f863595cf0c9c7",
    "zh:f7d7222bc796f362cf3b0211aca3ee256ac5a88cd52a1ddfbe918f01d8ed7970",
  ]
}

provider "registry.terraform.io/hashicorp/aws" {
  version     = "4.27.0"
  constraints = "4.27.0"
  hashes = [
    "h1:w3j7YomUQ9IfRp3MUuY0+hFX1T1cawZoj0Xsc1a46bU=",
    "zh:0f5ade3801fec487641e4f7d81e28075b716c787772f9709cc2378d20f325791",
    "zh:19ffa83be6b6765a4f821a17b8d260dd0f192a6c40765fa53ac65fd042cb1f65",
    "zh:3ac89d33ff8ca75bdc42f31c63ce0018ffc66aa69917c18713e824e381950e4e",
    "zh:81a199724e74992c8a029a968d211cb45277d95a2e88d0f07ec85127b6c6849b",
    "zh:9b12af85486a96aedd8d7984b0ff811a4b42e3d88dad1a3fb4c0b580d04fa425",
    "zh:a2e2c851a37ef97bbccccd2e686b4d016abe207a7f56bff70b10bfdf8ed1cbfd",
    "zh:baf844def338d77f8a3106b1411a1fe22e93a82e3dc51e5d33b766f741c4a6a3",
    "zh:bc33137fae808f91da0a9de7031cbea77d0ee4eefb4d2ad6ab7f58cc2111a7ff",
    "zh:c960ae2b33c8d3327f67a3db5ce1952315146d69dfc3f1b0922242e2b218eec8",
    "zh:f3ea1a25797c79c035463a1188a6a42e131f391f3cb714975ce49ccd301cda07",
    "zh:f7e77c871d38236e5fedee0086ff77ff396e88964348c794cf38e578fcc00293",
    "zh:fb338d5dfafab907b8608bd66cad8ca9ae4679f8c62c2435c2056a38b719baa2",
  ]
}

provider "registry.terraform.io/hashicorp/helm" {
  version = "2.6.0"
  hashes = [
    "h1:rGVucCeYAqklKupwoLVG5VPQTIkUhO7WGcw3WuHYrm8=",
    "zh:0ac248c28acc1a4fd11bd26a85e48ab78dd6abf0f7ac842bf1cd7edd05ac6cf8",
    "zh:3d32c8deae3740d8c5310136cc11c8afeffc350fbf88afaca0c34a223a5246f5",
    "zh:4055a27489733d19ca7fa2dfce14d323fe99ae9dede7d0fea21ee6db0b9ca74b",
    "zh:58a8ed39653fd4c874a2ecb128eccfa24c94266a00e349fd7fb13e22ad81f381",
    "zh:6c81508044913f25083de132d0ff81d083732aba07c506cc2db05aa0cefcde2c",
    "zh:7db5d18093047bfc4fe597f79610c0a281b21db0d61b0bacb3800585e976f814",
    "zh:8269207b7422db99e7be80a5352d111966c3dfc7eb98511f11c8ff7b2e813456",
    "zh:b1d7ababfb2374e72532308ff442cc906b79256b66b3fe7a98d42c68c4ddf9c5",
    "zh:ca63e226cbdc964a5d63ef21189f059ce45c3fa4a5e972204d6916a9177d2b44",
    "zh:d205a72d60e8cc362943d66f5bcdd6b6aaaa9aab2b89fd83bf6f1978ac0b1e4c",
    "zh:db47dc579a0e68e5bfe3a61f2e950e6e2af82b1f388d1069de014a937962b56a",
    "zh:f569b65999264a9416862bca5cd2a6177d94ccb0424f3a4ef424428912b9cb3c",
  ]
}

provider "registry.terraform.io/hashicorp/kubernetes" {
  version     = "2.8.0"
  constraints = "2.8.0"
  hashes = [
    "h1:UZCCMTH49ziz6YDV5oCCoOHypOxZWvzc59IfZxVdWeI=",
    "zh:0cf42c17c05ae5f0f5eb4b2c375dd2068960b97392e50823e47b2cee7b5e01be",
    "zh:29e3751eceae92c7400a17fe3a5394ed761627bcadfda66e7ac91d6485c37927",
    "zh:2d95584504c651e1e2e49fbb5fae1736e32a505102c3dbd2c319b26884a7d3d5",
    "zh:4a5f1d915c19e7c7b4f04d7d68f82db2c872dad75b9e6f33a6ddce43aa160405",
    "zh:4b959187fd2c884a4c6606e1c4edc7b506ec4cadb2742831f37aca1463eb349d",
    "zh:5e76a2b81c93d9904d50c2a703845f79d2b080c2f87c07ef8f168592033d638f",
    "zh:c5aa21a7168f96afa4b4776cbd7eefd3e1f47d48430dce75c7f761f2d2fac77b",
    "zh:d45e8bd98fc6752ea087e744efdafb209e7ec5a4224f9affee0a24fb51d26bb9",
    "zh:d4739255076ed7f3ac2a06aef89e8e48a87667f3e470c514ce2185c0569cc1fb",
    "zh:dbd2f11529a422ffd17040a70c0cc2802b7f1be2499e976dc22f1138d022b1b4",
    "zh:dbd5357082b2485bb9978bce5b6d508d6b431d15c53bfa1fcc2781131826b5d8",
  ]
}

provider "registry.terraform.io/hashicorp/null" {
  version = "3.1.1"
  hashes = [
    "h1:71sNUDvmiJcijsvfXpiLCz0lXIBSsEJjMxljt7hxMhw=",
    "zh:063466f41f1d9fd0dd93722840c1314f046d8760b1812fa67c34de0afcba5597",
    "zh:08c058e367de6debdad35fc24d97131c7cf75103baec8279aba3506a08b53faf",
    "zh:73ce6dff935150d6ddc6ac4a10071e02647d10175c173cfe5dca81f3d13d8afe",
    "zh:78d5eefdd9e494defcb3c68d282b8f96630502cac21d1ea161f53cfe9bb483b3",
    "zh:8fdd792a626413502e68c195f2097352bdc6a0df694f7df350ed784741eb587e",
    "zh:976bbaf268cb497400fd5b3c774d218f3933271864345f18deebe4dcbfcd6afa",
    "zh:b21b78ca581f98f4cdb7a366b03ae9db23a73dfa7df12c533d7c19b68e9e72e5",
    "zh:b7fc0c1615dbdb1d6fd4abb9c7dc7da286631f7ca2299fb9cd4664258ccfbff4",
    "zh:d1efc942b2c44345e0c29bc976594cb7278c38cfb8897b344669eafbc3cddf46",
    "zh:e356c245b3cd9d4789bab010893566acace682d7db877e52d40fc4ca34a50924",
    "zh:ea98802ba92fcfa8cf12cbce2e9e7ebe999afbf8ed47fa45fc847a098d89468b",
    "zh:eff8872458806499889f6927b5d954560f3d74bf20b6043409edf94d26cd906f",
  ]
}

provider "registry.terraform.io/kyma-incubator/kind" {
  version     = "0.0.11"
  constraints = "0.0.11"
  hashes = [
    "h1:NGeCtvJC1QytQtxFe5MOtAOSrmoXUFluVyAcPdKVEcs=",
    "zh:119013330d6681b266dedf034801fb8bb394d315b5deea217b59f364fc5749f0",
    "zh:126efb50568377cdf73736be3ec2c57fcfc6a8a8e5090c91ffcfdb1a3e6cdff8",
    "zh:2d9f33cdf1a9d60ac707ad50d4570637a81d38aee30215f926ea98e1d88461f1",
    "zh:c5f30187837b27196f679629a8fd5c25e9d2a12ada3179280e719d1460d7cb51",
    "zh:e1171caca0d134d8fc0119c50335bd21a1907744ad3a71a0c8e519ee89cd8443",
  ]
}

provider "registry.terraform.io/tehcyx/kind" {
  version     = "0.0.13"
  constraints = "0.0.13"
  hashes = [
    "h1:TfM80XFgeUlUQNeZ4/Rm2BJmgR1+hG4JQr9r5hNh6hU=",
    "zh:178205c4b9f1f54d4af7af9778b547c16fcf29879f45a7c6de172e6d54869030",
    "zh:b03cae2ca41c1a2fa829de9807f21e427ea15481a6cda95121ecc567cdf6bf36",
    "zh:bdfde68f75f2d0eb16148721f8b853905e42793af95e350f0c7ee861ed255726",
    "zh:da1f17510a2225d239892703dca62c635e26665812905e3186edeebc459c9ff0",
    "zh:f3c72fdb293266b27e802f16ed1c04b622df05ea5c48eb41813f4718606aa88a",
  ]
}
