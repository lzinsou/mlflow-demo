module "kind" {
  source = "../../modules/kind"
}

provider "helm" {
  kubernetes {
    host = module.kind.cluster.endpoint

    client_certificate     = module.kind.cluster.client_certificate
    client_key             = module.kind.cluster.client_key
    cluster_ca_certificate = module.kind.cluster.cluster_ca_certificate
  }
}

module "ecr" {
  source = "../../modules/ecr"

  aws_region           = var.image_repository_aws_region
  aws_account_role_arn = var.image_repository_aws_account_role_arn
}

locals {
  postgres_port = 5432
  storage_port  = 9000

  datalake_name = "datalake"
}

module "postgres" {
  source = "../../modules/postgres"

  name = "postgres"

  username = var.postgres_username
  password = var.postgres_password
  database = var.postgres_database
  port     = local.postgres_port
}

module "minio" {
  source = "../../modules/minio"

  name = "minio"

  access_key = var.storage_access_key
  secret_key = var.storage_secret_key
  api_port   = local.storage_port

  default_bucket_name = local.datalake_name
}

module "mlflow" {
  source = "../../modules/mlflow"

  chart_repository_uri      = var.chart_repository_uri
  chart_repository_username = var.chart_repository_username
  chart_repository_password = var.chart_repository_password

  image_registry_server   = module.ecr.endpoint
  image_registry_username = module.ecr.user_name
  image_registry_password = module.ecr.password

  name = "mlflow"

  db_type     = "postgresql"
  db_host     = "${module.postgres.helm_release_name}-postgresql"
  db_port     = local.postgres_port
  db_user     = var.postgres_username
  db_password = var.postgres_password
  db_database = var.postgres_database

  storage_host       = module.minio.helm_release_name
  storage_port       = local.storage_port
  storage_access_key = var.storage_access_key
  storage_secret_key = var.storage_secret_key

  artifacts_destination = "s3://${local.datalake_name}/mlflow/artifacts"
}
